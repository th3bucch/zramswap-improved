# zramswap-improved

Fork of [rpodgorny's zramswap script](https://aur.archlinux.org/packages/zramswap) merging Majaro forum's user [TriMoon improvements](https://forum.manjaro.org/t/improved-zramswap/34767):

>>>
 Here is an improved version that allows to:

  +  Manually set the size of the RAM used. SIZE=x and SIZE_TYPE=y where y can be KiB,MiB or GiB (Default).
  +  Prevent use of unsupported compression algorithms.
  +  Prevent the use of more as max RAM ZRAM_MAX_PERCENT=x (50% Default)
  +  RAM_PERCENT is used to config a percentage of max ram (20% Default). A non-zero SIZE= takes precedence.
>>>

___

## Table of Contents
[[_TOC_]]

## Description
Script to enable and configure a compressed `zram` device, on boot, used for swap. **For Arch Linux and derivates**  
More info on zram [here](https://en.wikipedia.org/wiki/Zram).

## Installation

```
git clone https://gitlab.com/th3bucch/zramswap-improved.git
cd zramswap-improved
makepkg -si
```

## Enable systemd service on boot

```
sudo systemctl enable --now zramswap.service
```

## Configuration 

For configuring the script edit `/etc/zramswap.conf` file.
Changes will take effect upon reboot or after stopping and restarting `zramswap` systemd service.

### Settings
| Option | Default | Description |
| :--- | :---: | :--- |
| SIZE | n/a | Manually define a fixed amount of memory used for the zram device. **Setting this option to a value greater than zero overrides `RAM_PERCENT`**. |
| SIZE_TYPE | GiB | Define the unit for `SIZE` option. Values: KiB, MiB or GiB. |
| RAM_PERCENT | 20 | Define the percentage of system RAM used for the zram device. |
| ZRAM_MAX_PERCENT | 50 | Limits the max amount of total system RAM used for the zram device. |
| ZRAM_COMPRESSION_ALGO | zstd | Compression algorithm for the zram device. See supported algorithms with `cat /sys/block/zram0/comp_algorithm` |


